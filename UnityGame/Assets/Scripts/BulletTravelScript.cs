﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTravelScript : MonoBehaviour
{
    public float bulletSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        /*this.transform.position = Vector2.MoveTowards(this.transform.position, target, maxDelta);*/
        transform.Translate(Vector2.right * bulletSpeed); 
       
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.layer.Equals(LayerMask.NameToLayer("Player"))
   && !other.gameObject.layer.Equals(LayerMask.NameToLayer("PlayerBullets")))
        {
            GameObject.Destroy(this.gameObject);
        }
        /*List<ContactPoint2D> c = new List<ContactPoint2D>();

        List<Collider2D> c2 = new List<Collider2D>();

        collision.GetContacts(c);
        collision.GetContacts(c2);
        
        if (c.Count > 0)
        {
            foreach(ContactPoint2D cc in c) {
                
                Debug.Log("ContactPoint2D: " + cc.otherCollider.gameObject.layer + " - " + cc.otherCollider.gameObject.name);
            }
            Debug.Log("----");
            if (!c[0].otherCollider.gameObject.layer.Equals(LayerMask.NameToLayer("Player"))
            && !c[0].otherCollider.gameObject.layer.Equals(LayerMask.NameToLayer("PlayerBullets")))
            {

                Debug.Log("I triggered!" + c[0].otherCollider.gameObject.layer);
                GameObject.Destroy(this.gameObject);
            }
        }

        if (c2.Count > 0)
        {
            foreach (Collider2D cc in c2)
            {

                Debug.Log("Collider2D: " + cc.gameObject.layer + " - " + cc.gameObject.name);
            }
            Debug.Log("----");
            if (!c2[0].gameObject.layer.Equals(LayerMask.NameToLayer("Player"))
            && !c2[0].gameObject.layer.Equals(LayerMask.NameToLayer("PlayerBullets")))
            {

                Debug.Log("I triggered!" + c2[0].gameObject.layer);
                GameObject.Destroy(this.gameObject);
            }
        }*/


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
    }
}
