﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerRoomChange : MonoBehaviour
{
    public Vector3 newCameraTarget;
    public bool isMoving;
    public Camera CameraToMove;
    public float moveTime = .5f;
    public GameObject playerObjectToMove;
    public Vector3 newPlayerPosition;
    public GameObject RoomToActivate;
    public GameObject RoomToDeactivate;

    // Start is called before the first frame update
    void Start()
    {
        isMoving = false;    
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            CameraToMove.transform.localPosition = new Vector3(
                Mathf.Lerp(CameraToMove.transform.localPosition.x, newCameraTarget.x, moveTime), 
                Mathf.Lerp(CameraToMove.transform.localPosition.y, newCameraTarget.y, moveTime), 
                CameraToMove.transform.localPosition.z);

            playerObjectToMove.transform.localPosition = new Vector3(
                Mathf.Lerp(playerObjectToMove.transform.localPosition.x, newPlayerPosition.x, moveTime),
                Mathf.Lerp(playerObjectToMove.transform.localPosition.y, newPlayerPosition.y, moveTime),
                playerObjectToMove.transform.localPosition.z);

            if (CameraToMove.transform.localPosition == newCameraTarget) { 
                isMoving = false;
                RoomToActivate.SetActive(true);
                RoomToDeactivate.SetActive(false);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isMoving)
            isMoving = true;
    }
}
