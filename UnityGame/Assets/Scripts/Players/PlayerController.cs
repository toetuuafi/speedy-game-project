﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    // Rotation
    private Animator player;

    // Movement
    [SerializeField]
    private float speed = 2f;

    // Health
    [SerializeField]
    private int _health = 3;
    [SerializeField]
    private int _maxHealth = 3;

    [SerializeField]
    private float invincibleAfterDamage = 3f; // 3 sec
    private float invincibleTimer;

    // animation stuff
    [SerializeField]
    private float _anim_invicibleFlashTime = 0.1f;
    private float _anim_invicibleTick = 0f;
    private List<SpriteRenderer> mySprites;

    public int health
    {
        get
        {
            return _health;
        }
        set
        {
            _health = value;
            refreshHealthUI();
        }
    }

    public int maxHealth
    {
        get
        {
            return _maxHealth;
        }
        set
        {
            _maxHealth = value;
            refreshHealthUI();
        }
    }

    private Rigidbody2D rbody;

    [SerializeField]
    private GameObject HealthUI;

    void refreshHealthUI()
    {
        if (HealthUI != null)
        {
            TextMeshProUGUI t = HealthUI.GetComponent<TextMeshProUGUI>();
            if (health > 0)
                t.text = string.Format("{0}/{1}", _health, _maxHealth);
            else
                t.text = "u ded.";
        }

    }

    public void ReceiveDamage(int amount)
    {
        // still invincible?
        if (invincibleTimer > 0f)
            return;

        // not ded?
        if (this.health > 0)
            this.health = this.health - amount;

        // do particle

        // do invincible
        invincibleTimer = invincibleAfterDamage;
    }

    void Awake()
    {
        player = GameObject.Find("Body").GetComponent<Animator>();
        rbody = GetComponent<Rigidbody2D>();
        mySprites = new List<SpriteRenderer>(GetComponentsInChildren<SpriteRenderer>());

        refreshHealthUI();
    }

    void FixedUpdate()
    {
        Movement();
        if (invincibleTimer > 0f)
        {
            invincibleTimer = invincibleTimer - Time.deltaTime;
            _anim_invicibleTick = _anim_invicibleTick + Time.deltaTime;
            if (_anim_invicibleTick >= _anim_invicibleFlashTime)
            {
                foreach(SpriteRenderer sr in mySprites)
                {
                    sr.enabled = !sr.enabled;
                }
                _anim_invicibleTick = 0f;
            }

            //invicible is over
            if (invincibleTimer <= 0f)
            {
                foreach(SpriteRenderer sr in mySprites)
                {
                    sr.enabled = true ;
                }
            }
        } 

    }

    void Movement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        Vector2 inputVector = Vector2.ClampMagnitude(new Vector2(horizontalInput, verticalInput), .25f);
        Vector2 currentpos = rbody.position;
        Vector2 newPos = currentpos + (inputVector * speed) * Time.deltaTime;
        rbody.MovePosition(newPos);
        float speedforanim = horizontalInput + verticalInput;
        if(speedforanim < 0)
        {
            speedforanim = -speedforanim;
        }
        player.SetFloat("Speed", speedforanim);
    }
}
