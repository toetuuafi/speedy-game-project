﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [SerializeField]
    private Animator weaponAnim;
    [SerializeField]
    private GameObject bulletPrefab;

    private Transform shootpoint;
    private float cooldown = .25f;
    private float cooldownCounter = 0f;
    private float bulletSpeed = .025f;

    private string weaponRotate;
    private string weaponRotateShoot;

    public Dictionary<string, Transform> bulletSpawnPoint;


    void Awake()
    {
        weaponAnim = GameObject.Find("weapon").GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        float angle = GetAngleFromMousePos();
        WeaponRotation(angle);
        WeaponFire(angle);
        CreatebulletSpawnPointDictionary();
    }

    private float GetAngleFromMousePos()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 5.23f;

        Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
        mousePos.x = mousePos.x - objectPos.x;
        mousePos.y = mousePos.y - objectPos.y;

        float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
        return angle;
    }

    void WeaponFire(float angle)
    {
        cooldownCounter = cooldownCounter - Time.deltaTime;
        if (cooldownCounter > 0f) return;
        if (Input.GetAxis("Fire1") != 0f)
        {
            cooldownCounter = cooldown;
            GameObject o = GameObject.Instantiate(bulletPrefab);
            BulletTravelScript bts = o.GetComponent<BulletTravelScript>();

            bts.transform.position = bulletSpawnPoint[weaponRotate].transform.position;
            bts.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
            //bts.target = new Vector2(0, 1);
            //bts.target = new Vector2(mousePos.x, mousePos.y);
            /*Rigidbody2D rb = bts.GetComponent<Rigidbody2D>();

            rb.velocity = mousePos * .5f;*/
            //bts.target = objectPos;

            //Debug.Log("ObjectPos: " + objectPos);
            //Debug.Log("Target: " + bts.target);


            bts.bulletSpeed = bulletSpeed;
        }
    }

    void WeaponRotation(float angle)
    {
        if(angle < 0)
        {
            angle = angle + 360;
        }
        if (angle >= 0f && angle <= 22.5f || angle >= 337.5f && angle <= 360f)
        {
            weaponRotate = "Right";
        }
        else if (angle >= 22.5f && angle <= 67.5f)
        {
            weaponRotate = "TopRight";
        }
        else if (angle >= 67.5f && angle <= 112.5f)
        {
            weaponRotate = "Top";
        }
        else if (angle >= 112.5f && angle <= 157.5f)
        {
            weaponRotate = "TopLeft";
        }
        else if (angle >= 157.5f && angle <= 202.5f)
        {
            weaponRotate = "Left";
        }
        else if (angle >= 202.5f && angle <= 247.5f)
        {
            weaponRotate = "DownLeft";
        }
        else if (angle >= 247.5f && angle <= 292.5)
        {
            weaponRotate = "Down";
        }
        else if (angle >= 292.5 && angle <= 337.5)
        {
            weaponRotate = "DownRight";
        }
        weaponRotateShoot = weaponRotate;
        if (Input.GetAxis("Fire1") != 0f)
        {
            weaponRotateShoot = weaponRotate + "Shoot";
        }
        weaponAnim.SetTrigger(weaponRotateShoot);
    }

    void CreatebulletSpawnPointDictionary()
    {
        bulletSpawnPoint = new Dictionary<string, Transform>();
        bulletSpawnPoint.Add("Down", GameObject.Find("_bulletspawnpointDown").transform);
        bulletSpawnPoint.Add("DownLeft", GameObject.Find("_bulletspawnpointDownLeft").transform);
        bulletSpawnPoint.Add("DownRight", GameObject.Find("_bulletspawnpointDownRight").transform);
        bulletSpawnPoint.Add("Top", GameObject.Find("_bulletspawnpointTop").transform);
        bulletSpawnPoint.Add("TopLeft", GameObject.Find("_bulletspawnpointTopLeft").transform);
        bulletSpawnPoint.Add("TopRight", GameObject.Find("_bulletspawnpointTopRight").transform);
        bulletSpawnPoint.Add("Right", GameObject.Find("_bulletspawnpointRight").transform);
        bulletSpawnPoint.Add("Left", GameObject.Find("_bulletspawnpointLeft").transform);
    }

}
