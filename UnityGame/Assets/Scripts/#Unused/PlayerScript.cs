﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            this.transform.localPosition = new Vector3(this.transform.localPosition.x - .1f, this.transform.localPosition.y, this.transform.localPosition.z);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            this.transform.localPosition = new Vector3(this.transform.localPosition.x + .1f, this.transform.localPosition.y, this.transform.localPosition.z);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y + .1f, this.transform.localPosition.z);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y - .1f, this.transform.localPosition.z);
        }

    }
    void OnCollisionEnter2D(Collision2D collision)
    {

        Debug.Log("collision");
    }
}
