﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    private Transform target;
    [SerializeField] private float speed;

    [SerializeField] private int maxhp;
    private int hp;

    void Awake()
    {
        target = FindObjectOfType<PlayerController>().GetComponent<Transform>();
        hp = maxhp;
    }

    void FixedUpdate()
    {
        // Movement
        transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);

        // Rotation
        if(transform.position.x <= target.position.x)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.layer.Equals(LayerMask.NameToLayer("Player")))
        {
            PlayerController p = col.GetComponentInParent<PlayerController>();
            p.ReceiveDamage(1);
        }
    }
}
