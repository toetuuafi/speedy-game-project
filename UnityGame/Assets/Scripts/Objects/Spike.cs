﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    private Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("Spike Attack");
        
        if (col.gameObject.layer.Equals(LayerMask.NameToLayer("Player"))){ 
            PlayerController p = col.GetComponentInParent<PlayerController>();
            p.ReceiveDamage(1);
            anim.SetTrigger("attack");
        }
    }
}
