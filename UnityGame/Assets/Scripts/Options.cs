﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    // DontDestroyOnLoad
    public static Options options;

    // Data
    [SerializeField][Range(0f, 1f)]
    private float music;
    [SerializeField][Range(0f, 1f)]
    private float sound;

    public void SetMusic(float volume)
    {
        music = volume;
    }
    public void SetSound(float volume)
    {
        sound = volume;
    }
    public float GetMusic()
    {
        return music;
    }
    public float SetSound()
    {
        return sound;
    }

    // UI
    [SerializeField]
    private Slider musicSlider;
    [SerializeField]
    private Slider soundSlider;

    public void UpdateMusic()
    {
        music = musicSlider.value;
        Save();
    }

    public void UpdateSound()
    {
        sound = soundSlider.value;
        Save();
    }

    void Awake()
    {
        if(options == null)
        {
            options = this;
            DontDestroyOnLoad(this);
            Load();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Load()
    {
        if(File.Exists(Application.dataPath + "/options.txt"))
        {
            string json = File.ReadAllText(Application.dataPath + "/options.txt");

            SaveOptions saveOptions = JsonUtility.FromJson<SaveOptions>(json);
            SetMusic(saveOptions.music);
            SetSound(saveOptions.sound);
            musicSlider.value = saveOptions.music;
            soundSlider.value = saveOptions.sound;
            //Debug.Log("OPTIONS : LOADED");

        }
        else
        {
            //Debug.Log("OPTIONS : NO SAVE FOUND");
        }
    }

    private void Save()
    {
        SaveOptions saveOptions = new SaveOptions
        {
            music = this.music,
            sound = this.sound
        };
        string json = JsonUtility.ToJson(saveOptions);
        File.WriteAllText(Application.dataPath + "/options.txt", json);
        //Debug.Log("OPTIONS : SAVED");
    }

    private class SaveOptions
    {
        public float music;
        public float sound;
    }
}
